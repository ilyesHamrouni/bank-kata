import domain.Account;

public class Main {
    public static void main(String[] args) {
        Account account = new Account(500);
        account.deposit(100);
        account.deposit(200);
        account.withdraw(-150);
        System.out.println(account.printStatement());
    }
}
