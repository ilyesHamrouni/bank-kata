package domain;

import java.util.Date;

public class Account {
    private int balance;
    private Records records;

    public Account(int balance){
        this.balance = balance;
        records = new Records();
    }

    public void deposit(int amount){
        this.balance= this.balance+amount;
        this.records.addStatement(new StatementRecord(balance,amount,new Date()));

    }
    public void withdraw(int amount){
        this.balance = this.balance+amount;
        this.records.addStatement(new StatementRecord(balance,amount,new Date()));
    }

    public String printStatement(){
        return this.records.printRecords();
    }
}
