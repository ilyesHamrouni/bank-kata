package domain;

import java.util.ArrayList;
import java.util.List;

public class Records {

    private  List<StatementRecord> records ;

    public Records(){
        this.records = new ArrayList<>();
    }

    public void  addStatement(StatementRecord statementRecord){
        this.records.add(statementRecord);
    }

    public List<StatementRecord> getRecords(){
        return this.records;
    }
    public String printRecords(){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("date        amount   balance");
        records.forEach(statementRecord -> stringBuilder.append(statementRecord.printStatementRecord()));
        return stringBuilder.toString();

    }
}
