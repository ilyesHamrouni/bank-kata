package domain;

import java.text.SimpleDateFormat;
import java.util.Date;

public class StatementRecord {

    public  int balance;
    public  int amount;
    public  Date date;

    public StatementRecord(int balance, int amount, Date date){
        this.amount=amount;
        this.balance=balance;
        this.date=date;
    }



    public String printStatementRecord(){
        return  "\n" + format(this.balance, this.amount, this.date);
    }
    private String format(int balance,int amount, Date date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy.MM.dd");

        if(amount>0){
            return String.format("%s    %d    %d",
                    format.format(date),
                    amount,
                    balance);
        }else{
            return String.format("%s   %d    %d",
                    format.format(date),
                    amount,
                    balance);
        }
    }
}
