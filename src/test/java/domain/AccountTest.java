package domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class AccountTest {

    Account account ;
    int balance;

    @BeforeEach
    public void init(){
        balance =500;
        account = new Account(balance);
    }

    @Test
    void deposit() {
        int amount = 300;
        account.deposit(amount);
        StatementRecord statementRecord = new StatementRecord(balance+amount,amount,new Date());
        Records records = new Records();
        records.addStatement(statementRecord);
        assertThat(account.printStatement()).isEqualTo(records.printRecords());
    }

    @Test
    void withdraw() {
        int amount = -300;
        account.deposit(amount);
        StatementRecord statementRecord = new StatementRecord(balance+amount,amount,new Date());
        Records records = new Records();
        records.addStatement(statementRecord);
        assertThat(account.printStatement()).isEqualTo(records.printRecords());
    }


    @Test
    void printStatementDeposit() {

        int amount = 300;
        account.deposit(amount);
        assertThat(account.printStatement()).isEqualTo("date        amount   balance\n2021.12.14    300    800");
    }

    @Test
    void printStatementWithdraw(){
        int withdrawAmount= -300;
        Account account =new Account(500);
        account.withdraw(withdrawAmount);
        assertThat(account.printStatement()).isEqualTo("date        amount   balance\n2021.12.14   -300    200");
    }
}
