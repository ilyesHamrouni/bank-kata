package domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import java.util.Date;

public class RecordsTest {

    Records records;

    @BeforeEach
    public void init(){
        records = new Records();
    }

    @Test
    public void addStatement(){
        StatementRecord statementRecord = new StatementRecord(500,300,new Date());
        records.addStatement(statementRecord);
        assertThat(records.getRecords().size()).isEqualTo(1);
    }

    @Test
    public void printRecords(){
        StatementRecord statementRecord = new StatementRecord(200,300,new Date());
        records.addStatement(statementRecord);
        assertThat(records.printRecords()).isEqualTo("date        amount   balance\n2021.12.14    300    200");

    }
}
