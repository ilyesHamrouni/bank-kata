package domain;

import org.junit.jupiter.api.Test;

import java.util.Date;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
class StatementRecordTest {

    @Test
    void printStatementRecord() {
        StatementRecord statementRecord = new StatementRecord(800,300,new Date());
        assertThat(statementRecord.printStatementRecord()).isEqualTo("\n2021.12.14    300    800");
    }
}